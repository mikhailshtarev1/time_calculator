package org.shtarev;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class CustomLocalTimeDeserializer extends JsonDeserializer<LocalTime> {


    @Override
    public LocalTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        final String date = jsonParser.getText();
        LocalTime localTime;
        if (date.length() == 4) {
            localTime = LocalTime.parse(date, DateTimeFormatter.ofPattern("H:mm"));
        } else {
            localTime = LocalTime.parse(date, DateTimeFormatter.ofPattern("HH:mm"));
        }
        return localTime;
    }
}
