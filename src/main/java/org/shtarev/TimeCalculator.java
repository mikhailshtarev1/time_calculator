package org.shtarev;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.NonNull;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;

public class TimeCalculator {

    public void getTime() throws IOException {
        final InputStream inputStream = App.class.getResourceAsStream("/resources/tickets.json");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        Tickets tickets = objectMapper.readValue(inputStream, Tickets.class);
        ArrayList<Long> listTimeTicket = new ArrayList<>();
        for (Ticket ticket : tickets.getTickets()) {
            final LocalDateTime dateStart = LocalDateTime.of(ticket.getDepartureDate(), ticket.getDepartureTime());
            final LocalDateTime dateFinish = LocalDateTime.of(ticket.getArrivalDate(), ticket.getArrivalTime());
            long avg = ChronoUnit.MINUTES.between(dateStart, dateFinish);
            listTimeTicket.add(avg);
        }
        System.out.println(getAVGTime(listTimeTicket));
        System.out.println(getPercentileTime(listTimeTicket));
    }

    private String getAVGTime(@NonNull final ArrayList<Long> listTicket) {
        final int time = (int) listTicket.stream().mapToInt(Long::intValue).average().getAsDouble();
        int hour = time / 60;
        int minute = time - hour * 60;
        return "Average flight time:  " + hour + " hours " + minute + " min";
    }

    private String getPercentileTime(@NonNull final ArrayList<Long> listTicket) {
        int listTSize = listTicket.size();
        if (listTSize < 2) {
            return "Insufficient data to calculate";
        }
        int x = new Double(0.9 * (listTSize - 1) + 1).intValue();
        Collections.sort(listTicket);
        double y = ((listTicket.get(x - 2)) + ((0.9 * (listTSize - 1) + 1) % x) *
                ((listTicket.get(x - 1)) - (listTicket.get(x - 2))));
        final int time = new Double(y).intValue();
        int hour = time / 60;
        int minute = time - hour * 60;
        return "90-th percentile of flight time between Vladivostok and Tel-Aviv: "
                + hour + " hours " + minute + " min";
    }
}
